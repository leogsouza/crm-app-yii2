<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\utilities;
use Symfony\Component\Yaml\Yaml;
use yii\web\ResponseFormatterInterface;

/**
 * Description of YamlResponseFormatter
 *
 * @author Leonardo
 */
class YamlResponseFormatter implements ResponseFormatterInterface
{
    const FORMAT = 'yaml';
    
    public function format($response)
    {
        $response->headers->set('Content-Type: application/yaml');
        $response->headers->set('Content-Disposition: inline');
        $response->content =  Yaml::dump($response->data);
        
    }
}
