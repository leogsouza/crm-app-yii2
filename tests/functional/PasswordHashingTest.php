<?php

use app\models\user\UserRecord;
class PasswordHashingTest extends \Codeception\TestCase\Test
{
        /**
         * @var FunctionalTester
         */
        protected $tester;

        protected function _before()
        {
        }

        protected function _after()
        {
        }

        /** 
         * @test 
         */
        public function PasswordIsHashedWhenSavingUser()
        {
            $user = $this->imagineUserRecord();
                
            $plainTextPassword = $user->password;
            
            $user->save();

            $savedUser = UserRecord::findOne($user->id);

            $security = new \yii\base\Security();
            $this->assertInstanceOf(get_class($user), $savedUser);
            $this->assertTrue(
                $security->validatePassword(
                    $plainTextPassword,
                    $savedUser->password
                )
            );
        }
        
        /**
         * 
         * @test
         */
        public function PasswordoIsNotRehashedAfterUpdateWithoutChangingPassword()
        {
            $user = $this->imagineUserRecord();
            $user->save();
            
            /** @var UserRecord $savedUser */
            $savedUser = UserRecord::findOne($user->id);
            $expectedHash = $savedUser->password;
            
            $savedUser->username = md5(time());
            $savedUser->save();
            
            /** @var UserRecord $updatedUser */
            $updatedUser = UserRecord::findOne($savedUser->id);
            
            $this->assertEquals($expectedHash, $savedUser->password);
            $this->assertEquals($expectedHash, $updatedUser->password);
            
        }
        
        private function imagineUserRecord()
        {
            $faker = \Faker\Factory::create();
            
            $user = new UserRecord();
            $user->username = $faker->word;
            $user->password = md5(time());
            
            return $user;
        }
}