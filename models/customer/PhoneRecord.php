<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models\customer;
use yii\db\ActiveRecord;


/**
 * Description of PhoneRecord
 *
 * @author Leonardo
 */
class PhoneRecord extends ActiveRecord
{
    public static function tableName()
    {
        return 'phone';
    }
    
    public function rules()
    {
        return [
            ['customer_id', 'number'],
            ['number', 'string'],
            [['customer_id', 'number'], 'required'],
        ];
    }
}
